﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class ProductsController : ApiController
    {

        Product[] products = new Product[] 
        { 
            new Product { PId = 1, Name = "Sugar", Category = "Groceries", Price = 25 }, 
            new Product { PId = 2, Name = "National Salt", Category = "Groceries", Price = 3.75M }, 
            new Product { PId = 3, Name = "Black pepper", Category = "Groceries", Price = 16.99M }, 
            new Product { PId = 4, Name = "Pepsi", Category = "Cold Drink", Price = 25.34M } 
        };

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.PId == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }




    }
}
